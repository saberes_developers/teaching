// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require jquery.validate
//= require jquery.validate.additional-methods
//= require bootstrap.min
//= require ace-elements.min
//= require ace.min
//= require_tree .


$(function(){
  $('.submenu').show();
    //TODO: fancybox jquery 1.9 patch
    //$('a[rel=lightbox]').fancybox(fancybox_options);
    //prevents backspace go back in browsers
    $(document).unbind('keydown').bind('keydown', function(event) {
        var doPrevent = false;
        if (event.keyCode === 8) {
            var d = event.srcElement || event.target;
            if ((d.tagName.toUpperCase() === 'INPUT' && (d.type.toUpperCase() === 'TEXT' || d.type.toUpperCase() === 'PASSWORD')) || d.tagName.toUpperCase() === 'TEXTAREA') {
                doPrevent = d.readOnly || d.disabled;
            } else {
                doPrevent = true;
            }
        }

        if (doPrevent) {
            event.preventDefault();
        }
    });
    //Code for cancel buttons
    $("body").delegate(".cancel_button", "click", function() {
        window.location.href = $(this).data("url");
    });
    
    $("body").delegate(".remote_cancel_button", "click", function() {
        $.fancybox.close();
    });
    

    //Double submit protection
    $('form.double_submit_protected ').submit(function() {
        // On submit disable its submit button
        $('form.double_submit_protected input[type=submit][disabled]:not(.undisable_submit)').addClass('disabled_submit');
        $('form.double_submit_protected input[type=submit]').attr('disabled', 'disabled');
    });

    $('form.validate_form').validate({
        errorClass : "client_error"
    });
    //update_table_colors();
    setTimeout("$('#wait').hide();", 300);
    jQuery.extend(jQuery.validator.messages, {
        required : "Este dato es obligatorio.",
        remote : "Please fix this field.",
        email : "Please enter a valid email address.",
        url : "Please enter a valid URL.",
        date : "Please enter a valid date.",
        dateISO : "Please enter a valid date (ISO).",
        number : "No es un número.",
        digits : "No es un número.",
        creditcard : "Please enter a valid credit card number.",
        equalTo : "Please enter the same value again.",
        accept : "Please enter a value with a valid extension.",
        maxlength : jQuery.validator.format("Please enter no more than {0} characters."),
        minlength : jQuery.validator.format("Please enter at least {0} characters."),
        rangelength : jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
        range : jQuery.validator.format("Please enter a value between {0} and {1}."),
        max : jQuery.validator.format("<br/>El valor debe ser menor o igual a {0}."),
        min : jQuery.validator.format("<br/> El valor debe ser mayor o igual a {0}.")
    });

})