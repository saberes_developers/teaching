class Country < ActiveRecord::Base
	validates :name, presence: true, uniqueness: true,length: {minimum: 1, maximum: 50}
	validates :language, presence: true, length: {minimum: 1, maximum: 15}
	validates :population, allow_nil: true, numericality: { only_integer: true, greater_than: 500}
	before_validation :strip_whitespace, :only => [:name, :language, :population]
	attr_accessible :language, :name, :population, :disabled
	def to_s
		self.name
	end

	def strip_whitespace
		self.name = self.name.strip
		self.language = self.language.strip
	end
end
