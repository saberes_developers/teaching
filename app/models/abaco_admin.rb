class AbacoAdmin < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :recoverable, :trackable, 
    :validatable, :token_authenticatable, :lockable, :timeoutable
  attr_accessible :email, :password

  has_many :admin_access_grants, :dependent => :delete_all

  def to_s
    self.email
  end


  self.token_authentication_key = "oauth_token"

  def apply_omniauth(omniauth)
    authentications.build(:provider => omniauth['provider'], 
      :uid => omniauth['uid'])
  end

  def self.find_for_token_authentication(conditions)
    where(["#{AdminAccessGrant.table_name}.access_token = ? AND (#{
      AdminAccessGrant.table_name}.access_token_expires_at IS NULL OR #{
      AdminAccessGrant.table_name}.access_token_expires_at > ?)", 
      conditions[token_authentication_key], Time.now]).
      joins(:admin_access_grants).select("#{self.table_name}.*").first
  end
end