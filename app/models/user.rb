class User < ActiveRecord::Base
  attr_accessible :email, :password, :password_confirmation, :locked_at


  def to_s
    self.email
  end

end 