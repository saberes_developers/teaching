module ApplicationHelper
	def list_id
		"list_#{suffix}"
	end

	def footer_id
		"footer_#{suffix}"
	end

	def cancel_button_tag(url = "", &block)
		button_tag t(:cancel), :type => "button", :class => (is_xhr? ? "remote_cancel_button btn" : "cancel_button btn"), :data => {:url => url}, &block
	end

  def edit_abaco_admin_registration_path(*args)
    ''
  end

  def icon(icon, before_text="", after_text="", options={})
    "<span>#{before_text}</span><i class='fa #{icon}'></i><span>#{after_text}</span>"
  end
end
