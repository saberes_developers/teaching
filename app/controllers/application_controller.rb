require 'ajax_default_resource'
require 'will_paginate_render'
class ApplicationController < ActionController::Base
  helper_method :get_user, :is_xhr?
  add_breadcrumb :home, "/"


  around_filter :manage_context

  def manage_context
    logger.debug(params.to_yaml)
    logger.info(request.session_options[:id])
    #Thread.current[:user] = session[:usuario_id]
    
    begin
      yield
    rescue 
      logger.exc($!, :error, -1)
      render :file => 'public/500.html', :status => 500, :layout => false
    ensure  
      #Thread.current[:user] = nil
      logger.info(flash[:notice]) if flash[:notice]  
    end
  end
  

  protect_from_forgery
  protected
  #Gets the controllers name translation 
  def self.get_controller_name
    I18n.t(self.name.underscore.split('/').last.sub!('_controller', ''))
  end
  #Lets know if the current request is ajax
  def is_xhr?
    request.xhr?
  end

  def get_user
    
  end
end
