class Admin::CountriesController < ApplicationController
	resource_controller
 	#add_breadcrumb get_controller_name, :collection_path
 	add_default_resource_methods

 	def disable_all
 		Country.update_all(disabled: true)
 		flash[:notice]=I18n.t(:countries_disabled)
 		redirect_to(collection_url)
 	end

 	def enable
 		load_object
 		@object.disabled=false
 		begin
	 		@object.save!
	 		flash[:notice]=I18n.t(:country_enabled)
 		rescue
 			logger.exc
 			flash[:notice]=I18n.t(:country_enabled_fail)
 		end
 		redirect_to(collection_url)
 	end
end
