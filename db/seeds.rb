# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
email='soporte@saberes.co'
if AbacoAdmin.where(:email=>email).blank?
	password='.!"/DeQs8'
	admin = AbacoAdmin.new 
	admin.email= email
	admin.password=password
	admin.password_confirmation =password
	admin.save!
end

if AppClient.where(:name=>'abaco_viewer').blank?
  object = AppClient.new 
  object.name = 'abaco_viewer'
  object.app_id = '4739adc46d051030814a445d286ffb90'
  object.app_secret = 'ee0339d3fa65279146315565aaae3ae13f4cc8b0ffcb3445e5a1c5d00e447ecd5752eccd80427c90c3ea91372a8e297efb6487876c848acf5a8018548d4a8a4f'
  object.save!
end
