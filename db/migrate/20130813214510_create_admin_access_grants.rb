class CreateAdminAccessGrants < ActiveRecord::Migration
  def change
    create_table :admin_access_grants do |t|
      t.string :code
      t.string :access_token
      t.string :refresh_token
      t.datetime :access_token_expires_at
      t.integer :abaco_admin_id
      t.integer :app_client_id
      t.string :state
      t.timestamps
    end
  end
end