class AddVisibleToCountries < ActiveRecord::Migration
  def change
    add_column :countries, :disabled, :boolean, :default=>false
  end
end
